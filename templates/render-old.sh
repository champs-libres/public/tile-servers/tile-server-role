#!/bin/bash

PLANET_TIMESTAMP=$(date -d "{{ osm_render_old_diff }}" +%d/%m/%Y)

echo "Rendering tiles older than ${PLANET_TIMESTAMP}"

render_old -n {{ osm_render_old_num_threads }} -z {{ osm_render_old_min_zoom }} -Z {{ osm_render_old_max_zoom }} --timestamp="${PLANET_TIMESTAMP}"

