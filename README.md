# Tile server ansible role

Install and configure a tile server on Debian 10.

This role will:

* create a gis user ;
* install postgresql and postgis, and create a gis db with extension postgis and hstore ;
* install mapnik from packages ;
* compile mod_tile and renderd ;
* configure mod_tile and renderd ;
* prepare the directories for generating the tiles ;
* install styles. Currently, only openstreetmap-carto is available ;
* configure apache ;
* prepare the script and update for services.

## Usage

### Prepare your disks

3 directories are in use:

* `/etc/mod_tile` will store the config files ;
* `/var/lib/postgresql` will store postgresql data. You should usually mount a ssd on this place ;
* `/var/lib/mod_tile` will store tiles cache, and temporary files for updates. This directory might be huge.

### Example role

```yaml
---
- hosts:
    - tiles_servers
  roles:
    - tile-server
```

### After installation

You should:

* import data ;
* once this is done, start the renderd daemon manually ;
* when everything is ok, configure the update process and start it ;

#### Import data

Download data, for instance from geofabrik servers.

**Note**: The following commands may take a while... You should run those into a screen session. Launch a screen using `screen` (preferably as gis user)`. Use `CTRL+A D` to detach, `screen -ls` to see the list of session, and `screen -r <pid number>` to re-attach.

```bash
# use the gis user
sudo su gis
# go to the directory where you will download the data
cd ~
# download data for europe
wget http://download.geofabrik.de/europe-latest.osm.pbf
wget http://download.geofabrik.de/europe-latest.osm.pbf.md5
wget http://download.geofabrik.de/europe-updates/state.txt
# check the file integrity
md5sum --check europe-latest.osm.pbf
```

Launch the import script. This script should be adapter to your hardware config:

```bash
# launch import command
osm2pgsql \
  -d gis `#database gis` \
  --create \
  --slim `#slim tables work for rendering` \
  -G `#how multipolygon are processed` \
  --hstore `#allow tags for which there are no explicit database columns to be used for rendering` \
  --tag-transform-script /etc/mod_tile/openstreetmap-carto/openstreetmap-carto.lua `#transform tags with lua script` \
  --style /etc/mod_tile/openstreetmap-carto/openstreetmap-carto.style `#select tags` \
  --cache 100000 `#cache in ram ~60% of available RAM` \
  --number-processes 24 `#number of processes` \
  --flat-nodes /var/lib/mod_tile/.import/flat-nodes `# keep node indexes in a file. Do not do this for small import` \
  europe-latest.osm.pbf
```

This should take a while, from a couple of hours to multiple days.

### Create the db indexes

**As `gis` user:

```bash
psql -d gis -f /etc/mod_tile/openstreetmap-carto/indexes.sql
```

### Launch the services

Launch renderd using

```
sudo systemctl start renderd
```

Debug information is available through `journalctl -u renderd`, or you can launch the daemon manually **as gis user**: 

```bash
sudo su gis
renderd --foreground -c /etc/renderd.conf
```

### Prepare your cache

You should also render the higher tiles levels immediatly:

**As gis user**:

```bash
render_list -a -z 0 -Z 9 -l 75 -n 30
```

And the lower tiles levels for your region:

```bash
cat /tmp/your_region.tiles | render_list -z 10 -Z 15 -n 30
```

Your region tiles may be generated using this script:

```python
import math

def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.asinh(math.tan(lat_rad)) / math.pi) / 2.0 * n)

  return (xtile, ytile)

# europe coordinates
max_x = 49.570313 
max_y = 71.580532
min_x = -25.312500
min_y = 31.503269

min_z = 9
max_z = 15 

for z in range(min_z, max_z+1):
    x1_tile, y1_tile = deg2num(max_y, max_x, z)
    x2_tile, y2_tile = deg2num(min_y, min_x, z)
    min_x_tile = x1_tile if x1_tile < x2_tile else x2_tile
    max_x_tile = x1_tile if x1_tile > x2_tile else x2_tile
    min_y_tile = y1_tile if y1_tile < y2_tile else y2_tile
    max_y_tile = y1_tile if y1_tile > y2_tile else y2_tile
    for tx in range(min_x_tile, max_x_tile):
        if tx % 8 != 0:
            continue
        for ty in range(min_y_tile, max_y_tile):
            if ty % 8 != 0:
                continue
            print("{} {} {}".format(tx, ty, z))
```

### Finalize and launch the updates

You should decide for an update policy :

* daily, for your region, using the geofabrik daily changes extracts ;
* minutely/hourly, applying a polygon file to filter the changes.

The configuration are described below.

Once done:

* launch the update process and timer:

    ```bash
    sudo systemctl start osm-update.timer
    sudo systemctl enable osm-updat.timer
    ```

You should launch manually the update process multiple times to get the lower replag delay possible:

```bash
sudo systemctl start osm-update.service
```

Logs are available using `journalctl`. 

For instance, to see the start time for the previous update launch in the last 12 hours: 

```
sudo journalctl -u osm-update.service --since -12h | grep replag
```

#### Configuration for daily updates:

For a daily update, the vars for role will be:

```yaml
# Set the frequency to hourly to launch the process as soon as the data is available:
osm_update_frequency: hourly

# parameters for osmosis download
update_osmosis_base_url: "http://download.geofabrik.de/europe-updates/"
update_osmosis_max_interval: 43200
```

Then, copy the `state.txt` file in `/var/lib/mod_tile/.osmosis/state.txt`

#### Minutely updates:

```yaml
# Set the frequency to every 10-20 minutes for not exhausting the hardware... 
osm_update_frequency: "*-*-* *:00,10,20,30,40,50:00"

# parameters for osmosis download: get at most one hour for each execution. This will help you to get data if one execution does fails
update_osmosis_max_interval: 3600

# a path to a poly file, which will filter the data.
# You can also store it manually in `/etc/mod_tile/current.poly` on the server
polyfile_url: "http://download.geofabrik.de/europe.poly"
```

Then, you should adapt the file `state.txt` and copy the adapted file in `/var/lib/mod_tile/.osmosis/state.txt`.

According this initial `state.txt` file:

```
# original OSM minutely replication sequence number 3834385
timestamp=2020-01-05T21\:59\:02Z
sequenceNumber=2484
```

Your `state.txt` file will be:

```
timestamp=2020-01-05T21\:59\:02Z
# we change the sequence number:
sequenceNumber=3834385
```

### Configure the expiration policy and launch the services

Tiles should expire and be re-rendered when their underlying data change. This may require a lot of time and CPU.

Two kind of policy are available here:

* immediatly re-render tiles after an update. This is more adapted to the daily update ;
* delete tiles or mark them as dirty after an update, an re-render them when they are requested, or when you decide to do it (for dirty tiles).

Usually, for daily updates:

* lower zoom levels (9-15) are re-rendered after update ;
* higher zoom levers are deleted.

For minutely udpates:

* lower zoom levels are set as dirty after update ;
* higher zoom levels are deleted ;
* once a day, the higher and old zoom levels are re-rendered if they are dirty.

**Note**: marking a tile as _dirty_ means that their atime is set in the past, i.e. 20 years ago.

Configuration variables:

```yaml
# parameters from expiry
update_expiry_min_zoom: '8'
update_expiry_max_zoom: '20'
update_expiry_delete_from: 16
update_expiry_num_threads: 8
update_expiry_touch_zoom: false
```

To activate the configuration of old tiles rendering:

* configure it ;
* launch and enable the service.

Configuration:

```yaml
# re-render old tiles at 23:00 
osm_render_old_frequency: "*-*-* 23:00:00"

osm_render_old_num_threads: 8
osm_render_old_min_zoom: 9
osm_render_old_max_zoom: 15
```

Note that, by default, the script will also re-render tiles older than one year in the defined zoom levels.

Launch and enable the timer:

```bash
sudo systemctl start render-old.timer
sudo systemctl enable renderd-old.timer
```

## Configure for your hardware

Multiple configurations parameters are available. You should adapt them to your hardware. 

See [the defaults vars](defaults/main.yml).

## Defaults variables

See [the defaults vars](defaults/main.yml), which is documented.

